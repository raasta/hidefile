/*
 * ----------------------------------------------
 * hidefile 0.1 by Raasta
 * ----------------------------------------------
 */

 /*
 * ----------------------------------------------
 * Config
 * ----------------------------------------------
 */

var config = {
    "basic_auth": false, // change to Basic authentication
    "user": "admin",
    "pass": "1234",
    "client_id": "longlongstring",// google client id
    "client_secret": "shorterstring", // google client secret
    "refresh_token": "longlongonetoo", // Authorization token
    "root":"teamdriveid" // you can use "root" if you want use only your account space
};


/*
 * ----------------------------------------------
 * Event handle
 * ----------------------------------------------
 */

const DEBUG = true
var request 

addEventListener("fetch", event => {
  try {
      event.respondWith(handleEvent(event))
  } catch (e) {
    if (DEBUG) {
      return event.respondWith(
          new Response(e.message || e.toString(), {
              status: 500,
          })
        )
    }
    event.respondWith(new Response("Internal Error", { status: 500 }))
  }
})

async function handleEvent(event) {
  request = event.request
  if (config.basic_auth && !doBasicAuth(event.request)) {
      return await unauthorized();
  }

  let url = new URL(request.url);
  let host = url.hostname;
  let pattern = new RegExp('^get.','i');
  if (pattern.test(host)){
    let id = url.pathname.split("/")[1].split("").reverse().join("");
      let range = request.headers.get('Range');
      return down(id,request,range)
  }else{
    let pathArray = url.pathname.split("/")
    let section = pathArray[1];
    var content = '';
    switch(section){
      case 'private':
        if (request.headers.get("Referer") != null){
          if (new URL(request.headers.get("Referer")).hostname == new URL(request.url).hostname) {
            var res = btoa(config.root)+'#@#'+btoa(await getAccessToken());
            return new Response(res, {status: 200, headers: {'Content-Type': 'text/html; charset=utf-8'}}) ;
          }
        }
        return Response.redirect('https://'+ new URL(request.url).hostname, 302)
      break;
      case 'info':
        return showInfo(request);
      break;
      default:
        return showMain(request);
    }
  }
}

/*
 * ----------------------------------------------
 * Google Drive
 * ----------------------------------------------
 */

async function getAccessToken(){
    const response = await fetch('https://www.googleapis.com/oauth2/v4/token', {
        method: 'POST',
        body: encodeQueryString({
            client_id: config.client_id,
            client_secret: config.client_secret,
            refresh_token: config.refresh_token,
            grant_type: 'refresh_token'
        }),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    const result = await response.json()
    if (result.error) {
        const error = new Error(result.error_description)
        error.status = response.status
        throw error
    }
    return result.access_token
}
    
function encodeQueryString(data){
    const result = []
    for (let k in data) {
        result.push(encodeURIComponent(k) + '=' + encodeURIComponent(data[k]))
    }
    return result.join('&')
}

async function down(id,request,range = ''){
    const accessToken = await getAccessToken();
    let ro  = {'method': 'GET', 'headers':{'authorization':'Bearer '+ accessToken,'Range':range}};
    return await fetch(`https://www.googleapis.com/drive/v3/files/${id}?alt=media`, ro).then(function(res) {
        if(!res.ok){
      return showDownError(request);
        }else{
            return res;
        }
    });
}

/*
 * ----------------------------------------------
 * Basic Authorization
 * ----------------------------------------------
 */

async function unauthorized() {
    return new Response('401 Not authorized', {
    headers: {
      'Content-Type': 'text/html; charset=utf-8',
      'WWW-Authenticate': 'Basic realm="", charset="UTF-8"',
      'Access-Control-Allow-Origin': '*'
    },
    status: 401
    });
}
  
function parseBasicAuth(auth) {
  try {
    return atob(auth.split(' ').pop()).split(':');
  } catch (e) {
    return [];
  }
}
  
function doBasicAuth(request) {
  const auth = request.headers.get('Authorization');
  if (!auth || !/^Basic [A-Za-z0-9._~+/-]+=*$/i.test(auth)) {
    return false;
  }
  const [user, pass] = parseBasicAuth(auth);
  return user === config.user && pass === config.pass;
}

/*
 * ----------------------------------------------
 * Views
 * ----------------------------------------------
 */

function showInfo(request){
  var tpl = mainTemplate(request);
  var content = `<div class="pt-2 pb-1 mb-1 border-bottom text-end border-secondary"><span class="me-2">Info</span></div>
        <div class="row m-3">
          <div class="qTitle border border-secondary rounded shadow bg-dark text-white mb-2 p-1 px-3">
            <strong>What is www.hidefile.ml ?</strong>
          </div>
          <div class="qAnswer px-4">
            It is a file hosting service based on the <a style="text-decoration: none;" class="link-secondary" href="https://en.wikipedia.org/wiki/KISS_principle" target="_blank">KISS principle</a>.<br>
            You upload a file and we give you a direct link to the file, that's all.
          </div>
        </div>

        <div class="row m-3">
          <div class="qTitle border border-secondary rounded shadow bg-dark text-white mb-2 p-1 px-3">
            <strong>Direct link?</strong>
          </div>
          <div class="qAnswer px-4">
            Yes, a direct link, with no intermediate pages full of ads.
          </div>
        </div>

        <div class="row m-3">
          <div class="qTitle border border-secondary rounded shadow bg-dark text-white mb-2 p-1 px-3">
            <strong>Any upload limit?</strong>
          </div>
          <div class="qAnswer px-4">            
            There is no size limit on uploading files, it has been tested with 200G files without any problem. The only limit is that you have to upload the files one by one. :)
          </div>
        </div>

        <div class="row m-3">
          <div class="qTitle border border-secondary rounded shadow bg-dark text-white mb-2 p-1 px-3">
            <strong>Can I recover a lost link?</strong>
          </div>
          <div class="qAnswer px-4">
            No, once created no reference is saved, please save the link provided well.
          </div>
        </div>

        <div class="row m-3">
          <div class="qTitle border border-secondary rounded shadow bg-dark text-white mb-2 p-1 px-3">
            <strong>It's free?</strong>
          </div>
          <div class="qAnswer px-4">
            Yes, totally free. If you want to help us in the footer you have a bitcoin address. Thank you.
          </div>
        </div>`;
  var html = tpl.replaceAll(/{{content}}/g, content);
  return new Response(html, {status: 200, headers: {'Content-Type': 'text/html; charset=utf-8'}}) ;
}

function showMain(request){
  var tpl = mainTemplate(request);
  var hostname = new URL(request.url).hostname;
  var getDomain = 'https://get.' + hostname.replaceAll(/www./g,'') + '/';
  var content = `
<div class="row justify-content-center text-center mt-5 p-5">
  <div id="uploadCell" class="d-block">
    <label for="uploadfile" class="border border-secondary rounded p-3 shadow bg-white" style="cursor:pointer;" alt="Upload a file" title="Upload a file">
        <i class="fa fa-cloud-upload fa-3x"></i> <span class="fs-1 fw-bold ms-2">Upload<span class="d-none d-lg-inline"> a file</span></span>
    </label>
    <input id="uploadfile" type="file" name="file" class="d-none"/>
  </div>
  <div id="uploadRes" class="d-none">
  </div>
</div>
<script type="text/javascript">
  document.getElementById("uploadfile").addEventListener("change", run, false);
  async function run(obj){
    const file = obj.target.files[0];
    if (file.name != "") {
      document.getElementById("uploadRes").classList.add('d-block');
        document.getElementById("uploadRes").classList.remove('d-none');
        const head = await getLocation(file);
        if (head != 'error'){
          const location = head.get("location")
            document.getElementById("uploadCell").innerHTML = '<label for="uploadfile" class="border border-secondary rounded p-3 shadow bg-white" alt="Uploading file" title="Uploading file"><i class="fa fa-sync-alt fa-spin fa-3x"></i> <span class="fs-1 fw-bold ms-2">Uploading<span class="d-none d-lg-inline"> file</span></span></label>'
            document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-3">File name: <i>' + file.name + '</i></div>'
          sendFile(file,location)
        }else{
            document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-3"><div class="alert alert-danger d-inline-block shadow "><strong>Error loading file <i>' + file.name + '</i>!</strong><br>Error creating upload link target.</div></div>';

        }
    }
  }

    function sendFile(file,location){
    var m = 1048576; // bytes
    const x = file.size;
    switch (true) {
        case (x < (m*50)):
            var chunkSize = m*1;
            break;
        case (x < (m*100)):
            var chunkSize = m*5;
            break;
        case (x < (m*500)):
            var chunkSize = m*25;
            break;
        default:
            var chunkSize = m*50;
            break;
    }
      //var chunkSize = (file.size < 524288000)?5242880:52428800; // bytes
      var offset     = 0;
      var startTime = new Date().getTime();
      function readChunk(file,offset,chunkSize,startTime){
        if(offset > file.size) return;
        var r = new FileReader();
        var blob = file.slice(offset, offset + chunkSize);
        r.onload = async function(e){
          var block = e.target.result
          const uint8Array = new Uint8Array(block)
          var end = offset + chunkSize >= file.size ? file.size   : offset + chunkSize;
          end -=1
          var chunk = {
                data: uint8Array,
                    length: uint8Array.length,
                    range: "bytes " + offset + "-" + end + "/" + file.size,
                    startByte: offset,
                    endByte: end,
                    total: file.size
          }
          var temp = Math.round((offset / file.size) * 100);
          var nowTime = new Date().getTime();
          nowTime -= startTime;
          if (offset != 0){
            var etaTime = getTimeReadable(((nowTime * file.size) / offset)-nowTime);
          }else{
            var etaTime = getTimeReadable(0);
          }
          document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-3">File name: <i>' + file.name + '</i></div><div class="row mt-3 justify-content-center"><div class="progress col-lg-4 col-md-8 p-0" style="height: 20px;"><div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: ' + temp +'%" aria-valuenow="' + temp +'" aria-valuemin="0" aria-valuemax="100">' + temp +'%</div></div></div><div id="extraInfo"></div>';
          document.getElementById("extraInfo").innerHTML ='<span class="small">Elapsed time: '+getTimeReadable(nowTime)+'&nbsp;&nbsp;&nbsp;Transfered: '+humanFileSize(offset)+'/'+humanFileSize(file.size)+'&nbsp;&nbsp;&nbsp;Estimated time: '+etaTime+'</span>';
          await fetch(location, {
                  method: "PUT",
                  body: uint8Array,
                  headers: {
                      "Content-Range": "bytes " + offset + "-" + end + "/" + file.size,
                      'Access-Control-Allow-Origin': '*',
                  }
              }).then(res => {
                  const status = res.status;
                  if (status == 308) {
                      //console.log(res)
                    //readChunk(file,offset,chunkSize);
                  } else if (status == 200) {
                      res.json().then(function(r){
                        //console.log(r);
                  document.getElementById("uploadCell").innerHTML = '<label for="uploadfile" class="border border-secondary rounded p-3 shadow " alt="File uploaded!" title="File uploaded!"><i class="fa fa-check fa-3x"></i> <span class="fs-1 fw-bold ms-2">File uploaded!</span></label>';
                    document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-4"><div class="alert alert-success d-inline-block shadow user-select-all">${getDomain}' + r.id.split("").reverse().join("") +'/' + encodeURIComponent(r.name) + '</div></div>';
                        } 
                      );
                  } else { 
                      res.json().then(err => {
                          console.log(err)
                          return;
                      });
                      return;
                  }
              }).catch(err => {
                  console.log(err)
            document.getElementById("uploadCell").innerHTML = '<label for="uploadfile" class="border border-secondary rounded p-3 shadow bg-white" style="cursor:pointer;" alt="Upload a file" title="Upload a file"><i class="fa fa-cloud-upload fa-3x"></i> <span class="fs-1 fw-bold ms-2">Upload<span class="d-none d-lg-inline"> a file</span></span></label><input id="uploadfile" type="file" name="file" class="d-none"/>'
              document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-3"><div class="alert alert-danger d-inline-block shadow "><strong>Error loading file <i>' + file.name + '</i>!</strong><br>' + err+'</div></div>';
                  return;
              });
          offset += chunkSize
          readChunk(file,offset,chunkSize,startTime);
        }
        r.onerror = function(e){
          console.log(e)
          document.getElementById("uploadCell").innerHTML = '<label for="uploadfile" class="border border-secondary rounded p-3 shadow bg-white" style="cursor:pointer;" alt="Upload a file" title="Upload a file"><i class="fa fa-cloud-upload fa-3x"></i> <span class="fs-1 fw-bold ms-2">Upload<span class="d-none d-lg-inline"> a file</span></span></label><input id="uploadfile" type="file" name="file" class="d-none"/>'
            document.getElementById("uploadRes").innerHTML = '<div class="text-center mt-3"><div class="alert alert-danger d-inline-block shadow "><strong>Error loading file <i>' + e.target.fileName + '</i>!</strong><br>' + e.target.error.message+'</div></div>';
        }
        r.readAsArrayBuffer(blob)
      }
      readChunk(file,offset,chunkSize,startTime);
    }

    function getData(){
      var request = new XMLHttpRequest();
        request.open('GET', '/private', false);
        request.send(null);
        if (request.status === 200) {
            return(request.responseText);
        }else{
            return 'error';
        }
    }
    
  async function getLocation(file){
    let data = getData().split('#@#');
    let metadata = {
            mimeType: file.type,
            name: file.name,
            parents:[atob(data[0])]
        };
        const endPoint = "https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable&supportsAllDrives=true&includeItemsFromAllDrives=true";
        return await fetch(endPoint, {
            method: "POST",
            body: JSON.stringify(metadata),
            headers: {
            Authorization: "Bearer " + atob(data[1]),
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',
            }
        }).then(res => {
            if (res.status != 200) {
                return "error";
            }
            return res.headers;
        }).catch(err => {
            return err;
        });
  }

  function getTimeReadable(millisec) {
        var seconds = (millisec / 1000).toFixed(0);
        var minutes = Math.floor(seconds / 60);
        var hours = "";
        if (minutes > 59) {
            hours = Math.floor(minutes / 60);
            //hours = (hours >= 10) ? hours : "0" + hours;
            minutes = minutes - (hours * 60);
            minutes = (minutes >= 10) ? minutes : "0" + minutes;
        }
        seconds = Math.floor(seconds % 60);
        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        if (hours != "") {
            return hours + ":" + minutes + ":" + seconds;
        }
        return minutes + ":" + seconds;
    }

    function humanFileSize(bytes, si=false, dp=1) {
      const thresh = si ? 1000 : 1024;
      if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
      }
      const units = si 
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
      let u = -1;
      const r = 10**dp;
      do {
        bytes /= thresh;
        ++u;
      } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);
      return bytes.toFixed(dp) + ' ' + units[u];
  }

    </script>
`;
  var html = tpl.replaceAll(/{{content}}/g, content);
  return new Response(html, {status: 200, headers: {'Content-Type': 'text/html; charset=utf-8'}}) ;
}

function showDownError(request){
  var tpl = mainTemplate(request,false);
  var content = `
  <div class="row justify-content-center text-center mt-5 p-5">
      <div id="uploadCell" class="d-block">
          <label for="uploadfile" class="border border-secondary rounded p-3  mt-3 alert alert-danger" alt="File not found" title="File not found"><i class="fa fa-times fa-3x"></i> <span class="fs-1 fw-bold ms-2">File not found</span></label>
      </div>
  </div>
  `;
  var html = tpl.replaceAll(/{{content}}/g, content)
  let url = new URL(request.url);
  let hostname = url.hostname;
  let domain = hostname.replaceAll(/get./g,'www.');
  return new Response(html,{status:200,headers:{'Content-Type':'text/html; charset=utf-8','refresh':'5; URL=https://'+domain}});
}

function mainTemplate(request,smenu = true){
  let url = new URL(request.url);
  let domain = url.hostname.replaceAll(/get./g,'www.');
  let domainName = domain.replaceAll(/www./g,'').replaceAll(/.ml/g,'');

  if (smenu){
    var menu = `<nav class="navbar navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="https://${domain}"><i class="fas fa-hippo"></i> ${domainName}</a>
        <ul class="nav">          
          <li class="nav-item">
            <a class="nav-link link-light" href="info" alt="Info" title="Info"><i class="fas fa-info-circle"></i></a>
          </li>
      </ul>
  </div>
</nav>
`;
  }else{
    var menu = `<nav class="navbar navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="https://${domain}"><i class="fas fa-hippo"></i> ${domainName}</a>
  </div>
</nav>
`;
  }

  var tpl = `<!--
         (\`-"""""-\`)    
          / 6   6 \\      ██     ██ ██     ██ ██     ██    ██   ██ ██ ██████  ███████ ███████ ██ ██      ███████    ███    ███ ██      
         |.--._.--.|      ██     ██ ██     ██ ██     ██    ██   ██ ██ ██   ██ ██      ██      ██ ██      ██         ████  ████ ██      
         /         \\     ██  █  ██ ██  █  ██ ██  █  ██    ███████ ██ ██   ██ █████   █████   ██ ██      █████      ██ ████ ██ ██      
         \\(o     o)/     ██ ███ ██ ██ ███ ██ ██ ███ ██    ██   ██ ██ ██   ██ ██      ██      ██ ██      ██         ██  ██  ██ ██      
          \`-u---u-'       ███ ███   ███ ███   ███ ███  ██ ██   ██ ██ ██████  ███████ ██      ██ ███████ ███████ ██ ██      ██ ███████ 
  -->\n<!doctype html>
  <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <title>Welcome to ${domainName}</title>
        <script src="https://kit.fontawesome.com/7e33c4f49d.js" crossorigin="anonymous"></script>
      </head>
      <body>
        ${menu}
        <div class="container-fluid mb-5">
          {{content}}
        </div>
        <nav class="navbar fixed-bottom navbar-dark bg-dark">
          <div class="container-fluid justify-content-center">
            <a class="link-light small" href="bitcoin:1ihaCDpk4vxwyzoCbsRrzoH8viv1tTBFy" style="text-decoration:none;" alt="BTC address" title="BTC address"><i class="fab fa-btc"></i> 1ihaCDpk4vxwyzoCbsRrzoH8viv1tTBFy</a>
          </div>
      </nav>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
      </body>
  </html>
  `;
  return tpl;
}